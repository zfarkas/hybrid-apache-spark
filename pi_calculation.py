import sys
import time
from random import random
from operator import add
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession

# Set the IP address of the Spark Master node below
SparkMasterIP="127.0.0.1"

# Number of samples
NUM_SAMPLES = 2 ** 32

# Number of partitions
partitions = 512


def f(_):
    x, y = random(), random()
    return 1 if x*x + y*y < 1 else 0


for i in range(0, 5):
    conf = SparkConf()
    conf.setMaster("local").setAppName("PythonPi")
    conf.setExecutorEnv("spark.blockManager.port", "13345")
    conf.set("spark.blockManager.port", "13345")
    sc = SparkContext(conf=conf, master="spark://"+SparkMasterIP+":7077")
    start_t = time.time();
    count = sc.parallelize(range(0, NUM_SAMPLES), partitions).map(f).reduce(add)
    end_t = time.time()
    sc.stop()
    print(end_t-start_t)
    print("Pi is roughly %f" % (4.0 * count / NUM_SAMPLES))
