#cloud-config

package_update: true
#package_upgrade: true


packages:
 - python3.7
 - openjdk-8-jdk
 - openjdk-8-jre
 - unzip
 - python3-pip
 - net-tools
 - mc

write_files:

- path: /tmp/installation.sh
  content: |
    #!/bin/bash

    # chage -d 2020-08-04 ubuntu
    set -ex
    HADOOP_VERSION={{variables.HADOOP_VERSION}}
    SPARK_VERSION={{variables.SPARK_VERSION}}
    SPARK_HADOOP_VERSION={{variables.SPARK_HADOOP_VERSION}}
    CONSUL_VERSION={{variables.CONSUL_VERSION}}
    CONSUL_TEMPLATE_VERSION={{variables.CONSUL_TEMPLATE_VERSION}}

    echo "Creating SPARKUSER starts."
    adduser --disabled-password --gecos "" sparkuser
    chown -R sparkuser:sparkuser /home/sparkuser
    echo "Creating SPARKUSER finished."


    # Turn off unattended upgrade
    sed -i 's/APT::Periodic::Unattended-Upgrade "1";/APT::Periodic::Unattended-Upgrade "0";/g' /etc/apt/apt.conf.d/20auto-upgrades

    su - sparkuser -c 'pip3 install pyspark==3.2.1'
    echo "Install requirement packages starts."

    echo "Install SPARK starts."
    wget -nc https://archive.apache.org/dist/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-hadoop$SPARK_HADOOP_VERSION.tgz -O /home/sparkuser/spark-$SPARK_VERSION-bin-hadoop$SPARK_HADOOP_VERSION.tgz
    tar -zxf /home/sparkuser/spark-$SPARK_VERSION-bin-hadoop$SPARK_HADOOP_VERSION.tgz  --directory /home/sparkuser
    mkdir /home/sparkuser/spark
    mv /home/sparkuser/spark-$SPARK_VERSION-bin-hadoop$SPARK_HADOOP_VERSION/* /home/sparkuser/spark
    rm -r /home/sparkuser/spark-$SPARK_VERSION-bin-hadoop$SPARK_HADOOP_VERSION.tgz /home/sparkuser/spark-$SPARK_VERSION-bin-hadoop$SPARK_HADOOP_VERSION
    echo "Install SPARK finished."

    echo "Install CONSUL starts."
    wget -nc "https://releases.hashicorp.com/consul/"$CONSUL_VERSION"/consul_"$CONSUL_VERSION"_linux_amd64.zip" -O /home/sparkuser/consul_"$CONSUL_VERSION"_linux_amd64.zip
    unzip -q /home/sparkuser/consul_"$CONSUL_VERSION"_linux_amd64.zip -d /home/sparkuser/consul/
    wget -nc "https://releases.hashicorp.com/consul-template/"$CONSUL_TEMPLATE_VERSION"/consul-template_"$CONSUL_TEMPLATE_VERSION"_linux_amd64.zip" -O /home/sparkuser/consul-template_"$CONSUL_TEMPLATE_VERSION"_linux_amd64.zip
    unzip -q /home/sparkuser/consul-template_"$CONSUL_TEMPLATE_VERSION"_linux_amd64.zip -d /home/sparkuser/consul/
    rm /home/sparkuser/consul_"$CONSUL_VERSION"_linux_amd64.zip /home/sparkuser/consul-template_"$CONSUL_TEMPLATE_VERSION"_linux_amd64.zip
    echo "Install CONSUL finished."

    echo -e "####################
    \e[92mInstallation DONE!!!\e[39m
    ####################"
  permissions: '755'

- path: /tmp/configuration.sh
  content: |
    #!/bin/bash

    set -ex
    MASTERIP=`hostname -I | col1`
    MASTERPUBLICIP=`curl http://169.254.169.254/2009-04-04/meta-data/public-ipv4`
    HOSTNAME=`hostname -s`


    echo "Configure SPARK starts."
    cp /home/sparkuser/spark/conf/spark-env.sh.template /home/sparkuser/spark/conf/spark-env.sh
    echo export SPARK_HOME=/home/sparkuser/spark >> /home/sparkuser/.bashrc
    chown -R sparkuser:sparkuser /home/sparkuser/spark
    echo "SPARK_MASTER_HOST={{getip('spark-master')}}" >> /home/sparkuser/spark/conf/spark-env.sh
    echo "SPARK_PUBLIC_DNS=$MASTERPUBLICIP" >> /home/sparkuser/spark/conf/spark-env.sh
    echo "SPARK_WORKER_PORT=12346" >> /home/sparkuser/spark/conf/spark-env.sh
    echo "SPARK_LOCAL_IP=$MASTERIP" >> /home/sparkuser/spark/conf/spark-env.sh
    echo "spark.driver.port 12345" >> /home/sparkuser/spark/conf/spark-defaults.conf
    echo "spark.driver.bindAddress $MASTERIP" >> /home/sparkuser/spark/conf/spark-defaults.conf
    echo "spark.driver.host $MASTERPUBLICIP" >> /home/sparkuser/spark/conf/spark-defaults.conf
    echo "spark.blockManager.port 13345" >> /home/sparkuser/spark/conf/spark-defaults.conf
    #echo "spark.authenticate true" >>/home/sparkuser/spark/conf/spark-defaults.conf
    #echo "spark.authenticate.secret {{variables.SPARK_SECRET}}" >>/home/sparkuser/spark/conf/spark-defaults.conf
    echo "Configure SPARK ends."
    chown -R sparkuser:sparkuser /home/sparkuser

    cat <<EOF > /home/sparkuser/consul/hosts.ctmpl
    127.0.0.1       localhost
    {{getip('spark-master')}} spark-master
    $MASTERIP       $HOSTNAME

    # The following lines are desirable for IPv6 capable hosts
    ::1     localhost ip6-localhost ip6-loopback
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters

    # Consul managed
    {% raw %}
    {{range service "spark-worker"}}
    {{index .ServiceMeta "public_ip"}} {{.Node}}{{end}}
    {% endraw %}
    EOF

    cat <<EOF > /home/sparkuser/consul/worker.hcl
    service {
      name = "spark-worker"
      id = "$HOSTNAME"
      port = 7077

      meta = {
        public_ip = "$MASTERPUBLICIP"
      }

    }
    EOF

    su - sparkuser -c 'mkdir /home/sparkuser/consul/logs'
    su - sparkuser -c 'mkdir /home/sparkuser/consul/data'


    echo "Launch CONSUL starts."
    systemctl start consul.service
    systemctl start consul-template-hosts.service
    echo "Launch CONSUL finished."

    while [[ `cat /etc/hosts | grep 'Consul' | wc -l` -eq 0 ]]; do
      echo "Waiting for /etc/host modification..."
      sleep 1
    done

    echo -e "#####################
    \e[92mConfiguration DONE!!!\e[39m
    #####################"
  permissions: '755'

- path: /tmp/start-services.sh
  content: |
    #!/bin/bash

    set -ex


    echo "Launch Spark starts."
    export SPARK_HOME=/home/sparkuser/spark
    MYIP=`hostname -I | col1`
    export PUBLICIP=`curl http://169.254.169.254/2009-04-04/meta-data/public-ipv4`
    /home/sparkuser/spark/sbin/start-slave.sh spark://{{getip('spark-master')}}:7077 -h $(hostname -s)
    #/home/sparkuser/spark/sbin/start-slave.sh spark://{{getip('spark-master')}}:7077 -h $PUBLICIP
    #/home/sparkuser/spark/sbin/start-slave.sh spark://{{getprivip('spark-master')}}:7077 -h $MYIP
    echo "Launch Spark finished."


    echo -e "###################
    \e[92mServices STARTED!!!\e[39m
    ###################"
  permissions: '755'

- path: /etc/systemd/system/consul.service
  content: |
    [Unit]
    Description=consul agent
    Requires=network-online.target
    After=network-online.target

    [Service]
    Restart=on-failure
    ExecStart=/bin/bash -c "/home/sparkuser/consul/consul agent -retry-join {{ getip('spark-master') }} -data-dir=/home/sparkuser/consul/data -config-file=/home/sparkuser/consul/worker.hcl -bind=$(hostname -I | col1) -client=0.0.0.0 -advertise=$(curl http://169.254.169.254/2009-04-04/meta-data/public-ipv4) >> /home/sparkuser/consul/logs/consul.log 2>&1"
    ExecReload=/bin/kill -HUP $MAINPID
    KillSignal=SIGTERM

    [Install]
    WantedBy=multi-user.target
  permissions: '644'

- path: /etc/systemd/system/consul-template-hosts.service
  content: |
    [Unit]
    Description=consul for hosts file
    Requires=network-online.target
    After=network-online.target

    [Service]
    Restart=on-failure
    ExecStart=/bin/bash -c "/home/sparkuser/consul/consul-template -consul-addr localhost:8500 -template \"/home/sparkuser/consul/hosts.ctmpl:/etc/hosts\" >> /home/sparkuser/consul/logs/consul-template-hosts.log 2>&1"
    ExecReload=/bin/kill -HUP $MAINPID
    KillSignal=SIGTERM

    [Install]
    WantedBy=multi-user.target
  permissions: '644'


runcmd:
- /tmp/installation.sh && /tmp/configuration.sh && su - sparkuser -c '/tmp/start-services.sh' && echo "SPARK WORKER DEPLOYMENT DONE." || echo -e "\e[91mPROBLEM OCCURED WITH THE INSTALLATION\e[39m"
