#!/bin/bash

echo "$MASTERIP spark-master" >> /etc/hosts
echo "export SPARK_WORKER_PORT=12346" >> /spark/conf/spark-env.sh
if [ ! -z "$_OCCOPUS_ALLOCATED_FQDN" ]; then
    echo "export SPARK_PUBLIC_DNS=$_OCCOPUS_ALLOCATED_FQDN" >> /spark/conf/spark-env.sh
fi
echo "spark.driver.port 12345" >> /spark/conf/spark-defaults.conf
echo "spark.blockManager.port 13345" >> /spark/conf/spark-defaults.conf
echo "spark.network.timeout 10s" >> /spark/conf/spark-defaults.conf
export SPARK_WORKER_PORT=12346
export SPARK_PUBLIC_DNS=$SPARK_PUBLIC_DNS

MYPUBLICIP=$(dig $_OCCOPUS_ALLOCATED_FQDN +short)
HOSTNAME=`hostname -s`
cat <<EOF > /consul/worker.hcl
service {
  name = "spark-worker"
  id = "$_OCCOPUS_ALLOCATED_FQDN"
  port = 7077
  meta = {
    public_ip = "$MYPUBLICIP"
  }
}
EOF

MYIP=`hostname -I | col1`
cat <<EOF > /consul/hosts.ctmpl
127.0.0.1 localhost
$MASTERIP spark-master
$MYIP     $HOSTNAME

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

# Consul managed
{{range service "spark-worker"}}
{{index .ServiceMeta "public_ip"}} {{.Node}}{{end}}
EOF

cat <<EOF > /consul/copy_hosts.sh
#!/bin/bash
cat /etc/hostsa > /etc/hosts
EOF
chmod +x /consul/copy_hosts.sh

mkdir /consul/logs/
/consul/consul agent -retry-join $MASTERIP -data-dir=/consul/data -config-file=/consul/worker.hcl -bind=$(hostname -I | col1) -client=0.0.0.0 -advertise=$MYPUBLICIP >> /consul/logs/consul.log 2>&1 &
/consul/consul-template -consul-addr localhost:8500 -template '/consul/hosts.ctmpl:/etc/hostsa:/consul/copy_hosts.sh' >> /consul/logs/consul-template-hosts.log 2>&1 &

/bin/bash /worker.sh
